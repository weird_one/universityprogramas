#ifndef STRING_STRING_H
#define STRING_STRING_H

#include <iostream>
#include <string>

using namespace std;

class String {
private:
    char* str;
public:
    String(const String& param);
    String(char* param);
    int lenght(char* str);
    void concut(const String& otherLine);
    bool isEqual(const String& firstString, const String& secondString);
    void replace(String& substring);
    friend ostream& operator<<(ostream& output, String& string);
    char *getStr() const;
};


#endif //STRING_STRING_H
