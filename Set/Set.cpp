#include "Set.h"

Set::Set()
{
    isCharInSet = 0;
}

Set::Set(const Set& set)
{
    isCharInSet = set.isCharInSet;
}

bool Set::isInSet(char symbol)
{
    int position = symbol - 'a';
    bool result;
    result = isCharInSet & (0x1 << position);
    return result;
}

void Set::addSymbol(char symbol)
{
    int position = symbol - 'a';
    isCharInSet = isCharInSet | (0x1 << position);
}

void Set::remove(char symbol)
{
    int position = symbol - 'a';
    isCharInSet = isCharInSet & ~(0x1 << position);
}

Set Set::combine(const Set& set)
{
    Set result;
    result.isCharInSet = isCharInSet | set.isCharInSet;
    return result;

}

Set Set::intersect(const Set& set)
{
    Set result;
    result.isCharInSet = isCharInSet & set.isCharInSet;
    return result;
}

Set Set::difference(const Set& set)
{
    Set result;
    result.isCharInSet = isCharInSet & ~set.isCharInSet;
    return result;
}

void Set::divide(Set& setLeft, Set& setRight, char symbol)
{
    setLeft.isCharInSet = 0;
    setRight.isCharInSet = 0;
    int position = symbol - 'a';
    for (int i = 0; i < position; i++)
    {
        setLeft.isCharInSet |= isCharInSet & (0x1 << i);
    }
    for (int i = position + 1; i < charAmount; i++)
    {
        setRight.isCharInSet |= isCharInSet & (0x1 << i);
    }
}

char Set::searchMin()
{
    int minIndex = -1;
    for (int i = 0; i < charAmount; i++)
    {
        if (isCharInSet & (0x1 << i))
        {
            minIndex = i;
            break;
        }
    }
    if (minIndex == -1)
    {
        throw "no element found!";
    }
    char result = minIndex + 97;
    return result;
}
