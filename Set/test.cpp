
#include <gtest/gtest.h>
#include "Set.h"

TEST(SetTest, AddTest)
{
    Set set;
    set.addSymbol('a');
    set.addSymbol('b');
    ASSERT_TRUE(set.isInSet('b'));
}

TEST(SetTest, DeleteTest)
{
    Set set;
    set.addSymbol('a');
    set.addSymbol('b');
    set.remove('a');
    ASSERT_FALSE(set.isInSet('a'));
}

TEST(SetTest, CombineTest)
{
    Set firstSet;
    Set secondSet;
    firstSet.addSymbol('a');
    firstSet.addSymbol('b');
    secondSet.addSymbol('b');
    secondSet.addSymbol('c');
    Set combineSet = firstSet.combine(secondSet);
    ASSERT_TRUE(combineSet.isInSet('a'));
    ASSERT_TRUE(combineSet.isInSet('b'));
    ASSERT_TRUE(combineSet.isInSet('c'));
}

TEST(SetTest, IntersectTest)
{
    Set firstSet;
    Set secondSet;
    firstSet.addSymbol('a');
    firstSet.addSymbol('b');
    secondSet.addSymbol('b');
    secondSet.addSymbol('c');
    Set combineSet = firstSet.intersect(secondSet);
    ASSERT_TRUE(combineSet.isInSet('b'));
    ASSERT_FALSE(combineSet.isInSet('c'));
}

TEST(SetTest, DifferenceTest)
{
    Set firstSet;
    Set secondSet;
    firstSet.addSymbol('a');
    firstSet.addSymbol('b');
    secondSet.addSymbol('b');
    secondSet.addSymbol('c');
    Set combineSet = firstSet.difference(secondSet);
    ASSERT_FALSE(combineSet.isInSet('b'));
    ASSERT_TRUE(combineSet.isInSet('a'));
}

TEST(SetTest, DivideTest)
{
    Set set;
    set.addSymbol('a');
    set.addSymbol('b');
    set.addSymbol('c');
    set.addSymbol('d');
    set.addSymbol('e');
    set.addSymbol('f');
    Set right;
    Set left;
    left.addSymbol('e');
    set.divide(left, right, 'd');
    ASSERT_TRUE(left.isInSet('c'));
    ASSERT_FALSE(left.isInSet('e'));
    ASSERT_TRUE(right.isInSet('e'));
    ASSERT_FALSE(right.isInSet('a'));
}

TEST(SetTest, SmallestTest)
{
    Set set;
    set.addSymbol('a');
    set.addSymbol('b');
    set.addSymbol('c');
    set.addSymbol('d');
    set.addSymbol('e');
    set.addSymbol('f');
    ASSERT_EQ('a', set.searchMin());
}



