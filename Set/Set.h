#ifndef SET_SET_H
#define SET_SET_H


class Set {
private:
    const int charAmount = 'z' - 'a' + 1;
    int isCharInSet;
public:
    Set();
    Set(const Set& set);
    bool isInSet(char symbol);
    void addSymbol(char symbol);
    void remove(char symbol);
    Set combine(const Set& set);
    Set intersect(const Set& set);
    Set difference(const Set& set);
    void divide(Set& setLeft, Set& setRight, char symbol);
    char searchMin();
};


#endif //SET_SET_H