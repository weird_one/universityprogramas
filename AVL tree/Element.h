#ifndef AVL_TREE_ELEMENT_H
#define AVL_TREE_ELEMENT_H

#include <iostream>

using namespace std;

class Element {
public:
    int key;
    string data;
    Element* left;
    Element* right;
    int  height;
    Element();
    Element(int key, const string &data);
};


#endif //AVL_TREE_ELEMENT_H
