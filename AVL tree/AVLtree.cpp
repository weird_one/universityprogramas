#include "Element.h"
#include "AVLtree.h"

AVLtree::AVLtree()
{
    root = nullptr;
}


void AVLtree::insert(int tkey, string tdata)
{
    root = insert(root, tkey, tdata);
}

string AVLtree::find(int key)
{
    if (root == nullptr)
    {
        throw "Sorry, but tree is empty";
    }
    Element* slider = root;
    while (slider != nullptr && slider->key != key)
    {
        if (key > slider->key)
        {
            slider = slider->right;
        }
        else
        {
            slider = slider->left;
        }
    }
    if (slider == nullptr)
    {
        throw "There are no such element";
    }
    return slider->data;
}

int AVLtree::getHeight()
{
    return root->height;
}

void AVLtree::remove(int key)
{
    root = remove(root, key);
}


int AVLtree::height(Element *toFingHeight)
{
    return toFingHeight ? toFingHeight->height : -1;
}

int AVLtree::bfactor(Element *toFindFactor)
{
    return height(toFindFactor->left) - height(toFindFactor->right);
}

void AVLtree::fixheight(Element *toBalance)
{
    int  heightLeft = height(toBalance->left);
    int  heightRight = height(toBalance->right);
    toBalance->height = (heightLeft > heightRight ? heightLeft : heightRight) + 1;
}

Element *AVLtree::findmin(Element *toFind)
{
    Element* slider = toFind;
    while (slider->left)
    {
        slider = slider->left;
    }
    return slider;
}

Element *AVLtree::removemin(Element *toRemove)
{
    if (toRemove->left == nullptr)
    {
        return toRemove->right;
    }
    toRemove->left = removemin(toRemove->left);
    return balance(toRemove);
}

Element *AVLtree::rotateright(Element *toRotate)
{
    Element* rotateHelper = toRotate->left;
    toRotate->left = rotateHelper->right;
    rotateHelper->right = toRotate;
    fixheight(toRotate);
    fixheight(rotateHelper);
    return rotateHelper;
}

Element *AVLtree::rotateleft(Element *toRotate)
{
    Element* rotateHelper = toRotate->right;
    toRotate->right = rotateHelper->left;
    rotateHelper->left = toRotate;
    fixheight(toRotate);
    fixheight(rotateHelper);
    return rotateHelper;
}

Element *AVLtree::balance(Element *toBalance)
{
    fixheight(toBalance);
    if (bfactor(toBalance) == 2)
    {
        if (bfactor(toBalance->left) == -1)
        {
            toBalance->left = rotateleft(toBalance->left);
        }
        return rotateright(toBalance);
    }
    else if (bfactor(toBalance) == -2)
    {
        if (bfactor(toBalance->right) == 1)
        {
            toBalance->right = rotateright(toBalance->right);
        }
        return rotateleft(toBalance);
    }
    return toBalance;
}

Element *AVLtree::insert(Element *toInsert, int &key, string &data)
{
    if (toInsert == nullptr)
    {
        return new Element(key, data);
    }
    if (key < toInsert->key)
    {
        toInsert->left = insert(toInsert->left, key, data);
    }
    else
    {
        toInsert->right = insert(toInsert->right, key, data);
    }
    return balance(toInsert);
}

Element *AVLtree::remove(Element *toRemove, int &key)
{
    if (toRemove == nullptr)
    {
        return nullptr;
    }
    if (key < toRemove->key)
    {
        toRemove->left = remove(toRemove->left, key);
    }
    else if (key > toRemove->key)
    {
        toRemove->right = remove(toRemove->right, key);
    }
    else
    {
        Element* rightRemove = toRemove->right;
        Element* leftRemove = toRemove->left;
        delete toRemove;
        if (rightRemove == nullptr)
        {
            return leftRemove;
        }
        Element* min = findmin(rightRemove);
        min->right = removemin(rightRemove);
        min->left = leftRemove;
        return balance(min);
    }
    return balance(toRemove);
}

void AVLtree::find(Element *toFind, string &value, int &result)
{
    if (toFind == nullptr)
    {
        return;
    }
    if (toFind->data == value)
    {
        result = toFind->key;
    }
    find(toFind->left, value, result);
    find(toFind->right, value, result);
}

AVLtree::~AVLtree()
{
    while (root != nullptr)
    {
        remove(root->key);
    }
}
