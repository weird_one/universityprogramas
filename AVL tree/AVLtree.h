#pragma once
#include<string>
#include<exception>
#include<iostream>
#include<vector>
#include "Element.h"

using namespace std;

class AVLtree
{
	Element* root;
	int  height(Element* toFingHeight);
	int bfactor(Element* toFindFactor);
	void fixheight(Element* toBalance);
	Element* findmin(Element* toFind);
	Element* removemin(Element* toRemove);
	Element* rotateright(Element* toRotate);
	Element* rotateleft(Element* toRotate);
	Element* balance(Element* toBalance);
	Element* insert(Element *toInsert, int &key, string &data);
	Element* remove(Element *toRemove, int &key);
	void find(Element *toFind, string &value, int &result);
public:
    AVLtree();
    ~AVLtree();
	void insert(int tkey, string tdata);
	string find(int key);
	int getHeight();
	void remove(int key);
};
