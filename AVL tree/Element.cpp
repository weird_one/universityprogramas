//
// Created by Roger on 5/22/2017.
//

#include "Element.h"

Element::Element()
{
    right = nullptr;
    left = nullptr;
    height = 0;
}

Element::Element(int key, const string &data) :
        key(key),
        data(data)
{
    right = nullptr;
    left = nullptr;
    height = 0;
}
