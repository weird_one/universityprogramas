cmake_minimum_required(VERSION 3.6)
project(AVL_tree)

set(CMAKE_CXX_STANDARD 11)

set(SOURCE_FILES main.cpp AVLtree.cpp Element.cpp Element.h AVLtree.h test.cpp)
add_executable(AVL_tree ${SOURCE_FILES})

add_subdirectory(lib/googletest/googletest)
include_directories(lib/googletest/googletest/include/gtest)
target_link_libraries(AVL_tree gtest gtest_main)