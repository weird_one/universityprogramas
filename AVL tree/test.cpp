#include <gtest/gtest.h>
#include "AVLtree.h"

TEST(AvlTree, insert)
{
    AVLtree tree;
    tree.insert(4, "dog");
    tree.insert(6, "cat");
    tree.insert(8, "dragon");
    tree.insert(9, "wolf");
    tree.insert(3, "forsyte");
    tree.insert(5, "John");
    tree.insert(2, "two");
    tree.insert(1, "one");
    ASSERT_EQ("dog", tree.find(4));
    ASSERT_EQ("two", tree.find(2));
}

TEST(AvlTree, removeAndBalance)
{
    AVLtree tree;
    tree.insert(4, "dog");
    tree.insert(6, "cat");
    tree.insert(8, "dragon");
    tree.insert(10, "wildDragon");
    tree.insert(9, "wolf");
    tree.insert(3, "forsyte");
    tree.insert(5, "John");
    tree.insert(2, "two");
    tree.insert(1, "one");
    ASSERT_EQ("John", tree.find(5));
    int oldHeight = tree.getHeight();
    tree.remove(5);
    tree.remove(4);
    int newHeight = tree.getHeight();
    ASSERT_ANY_THROW(tree.find(5));
    ASSERT_TRUE(oldHeight == 3 && newHeight == 2);
}