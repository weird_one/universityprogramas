#include <gtest/gtest.h>
#include "../src/Stack.h"

TEST(StackTest, pushTest)
{
    Stack stack = Stack();
    stack.push(20);
    ASSERT_EQ(stack.getLastElement(), 20);
}

TEST(StackTest, PopTest)
{
    Stack stack = Stack();
    stack.push(20);
    stack.push(13);
    stack.push(15);
    stack.pop();
    ASSERT_EQ(stack.getLastElement(), 13);
}

TEST(StackTest, EmptyTest)
{
    Stack stack = Stack();
    ASSERT_TRUE(stack.isEmpty());
}

TEST(StackTest, EmptyPushPopTest)
{
    Stack stack = Stack();
    stack.push(20);
    stack.pop();
    ASSERT_TRUE(stack.isEmpty());
}

TEST(StackTest, IncreaseStack)
{
    Stack stack = Stack();
    stack.push(10);
    stack.push(15);
    stack.push(25);
    stack.push(18);
    stack.push(60);
    stack.push(65);
    stack.push(12);
    ASSERT_EQ(stack.getLastElement(), 12);
}