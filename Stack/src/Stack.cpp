#include "Stack.h"
#include <iostream>
using namespace std;

void Stack::push(int var)
{
    stack[count] = var;
    count ++;
    if(count == size)
    {
        increaseStack();
    }
    cout << "Елемент " << var << " додано в стек." << endl;
}

void Stack::pop()
{
    count --;
    cout << "Останній елемент видалено з стеку." << endl;
}

bool Stack::isEmpty()
{
    if (count == 0)
    {
        return true;
    }
    return false;
}

Stack::Stack(int size) :
        size(size)
{
    stack = new int[size];
    count = 0;
}

Stack::Stack()
{
    size = 3;
    stack = new int[size];
    count = 0;
}

int Stack::getLastElement()
{
    return stack[count - 1];
}

void Stack::increaseStack()
{
    int* largerStack = new int[size + 3];
    for(int i = 0; i < size; i++)
    {
        largerStack[i] = stack[i];
    }
    delete[] stack;
    stack = largerStack;
    size += 3;
}

Stack::~Stack()
{
    delete[] stack;
}
