#ifndef STACK_STACK_H
#define STACK_STACK_H


class Stack
{
private:
    int* stack;
    int size;
    int count;
public:
    Stack(int size);
    Stack();
    void push(int var);
    void pop();
    bool isEmpty();
    int getLastElement();
    void increaseStack();
    ~Stack();
};

#endif //STACK_STACK_H
