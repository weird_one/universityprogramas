//
// Created by wierdone on 29.03.17.
//

#ifndef VIRTUALFUNCTION_FILEREADER_H
#define VIRTUALFUNCTION_FILEREADER_H


#include "../product/Product.h"

class FileReader {
public:
    int size;
    Product** product;
    Product** read();
};


#endif //VIRTUALFUNCTION_FILEREADER_H
