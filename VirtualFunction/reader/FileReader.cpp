#include <fstream>
#include "FileReader.h"
#include "../product/Drink.h"
#include "../product/Food.h"

Product** FileReader::read() {

    ifstream myFile("/home/wierdone/CLionProjects/VirtualFunction/products.txt", ios::in);
    myFile >> size;
    product = new Product*[size];
    string line;
    double length;
    int price;
    int amount;
    bool type;
    if(!myFile.is_open()){
        cout << "Cant open the file" << endl;
        size = 0;
        return new Product*[0];
    }
    else{
        for(int i=0; i<size; i++){
            myFile >> line;
            myFile >> price;
            myFile >> amount;
            myFile >> length;
            myFile >> type;
            if(line == "drink")
            {
                product[i] = new Drink(price, line, amount, length, type);
            }
            else
            {
                product[i] = new Food(price, line, amount, length, type);
            }
        }
    }
    return product;
}
