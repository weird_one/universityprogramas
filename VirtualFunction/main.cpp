#include <iostream>
#include "product/Product.h"
#include "reader/FileReader.h"
#include "product/Drink.h"

void sort(Product** products, int size)
{
    for(int i=0; i<size; i++){
        for(int j=0; j<size-1-i; j++){
            if(products[j]->getType() > products[j+1]->getType()){
                swap(products[j], products[j+1]);
            }
        }
    }

    for(int i=0; i<size; i++){
       for(int j=0; j<size-1-i; j++){
           if(products[j]->getType() == "drink" && products[j+1]->getType() == "drink"){
               if(products[j]->checkType() > products[j+1]->checkType()){
                   swap(products[j], products[j+1]);
               }
           }
           if(products[j]->getType() == "food" && products[j+1]->getType() == "food"){
               if(products[j]->checkType() > products[j+1]->checkType()){
                   swap(products[j], products[j+1]);
               }
           }
       }
    }
}

int main() {
    FileReader reader;
    Product** products;
    products = reader.read();
    sort(products, reader.size);
    for(int i=0; i<5; i++){
        products[i]->print();
        cout << endl;
    }
    return 0;
}