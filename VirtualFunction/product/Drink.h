#include "Product.h"

class Drink : public Product {
private:
    double liters;
    bool achogol;
public:
    Drink(int price, const string &type, int number, double liters, bool achogol);
    Drink();
    string checkType() override;
    void print() override;
};


