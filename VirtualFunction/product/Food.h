#include "Product.h"

class Food : public Product
{
private:
    double calory;
    bool harm;
public:
    Food(int price, const string &type, int number, double calory, bool harm);
    Food();
    string checkType() override;
    void print() override;
};


