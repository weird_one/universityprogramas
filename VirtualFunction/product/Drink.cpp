#include "Drink.h"

Drink::Drink(int price, const string &type, int number, double liters, bool achogol) :
        Product(price, type, number),
        liters(liters),
        achogol(achogol)
{

}

Drink::Drink()
{
    liters = 0;
    achogol = false;
}

string Drink::checkType() {
    if(achogol){
        return "alchogol";
    }
    return "soft";
}

void Drink::print() {
    cout << "Price is:"
           << this->price
           << ", type is: "
           << this->type
           << ", amount is: "
           << this->amount
           << ", liters is: "
           << this->liters;
    if(this->achogol){
        cout << ", is alcogol";
    }
    else
    {
        cout << ", is soft";
    }
}
