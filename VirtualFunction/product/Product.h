#ifndef VIRTUALFUNCTION_PRODUCT_H
#define VIRTUALFUNCTION_PRODUCT_H

#include <iostream>

using namespace std;

class Product {
protected:
    int price;
    string type;
    int amount;
public:
    Product(int price, const string &type, int number);
    Product();
    const string &getType();
    virtual string checkType() = 0;
    virtual void print() = 0;
};

#endif //VIRTUALFUNCTION_PRODUCT_H