#include "Food.h"

string Food::checkType() {
    if(harm){
        return "harmful";
    }
    return "harmless";
}

Food::Food(int price, const string &type, int number, double calory, bool harm) :
        Product(price, type, number),
        calory(calory),
        harm(harm)
{

}

Food::Food()
{
    calory = 0;
    harm = false;
}

void Food::print() {
    cout << "Price is:"
         << this->price
         << ", type is: "
         << this->type
         << ", amount is: "
         << this->amount
         << ", calory is: "
         << this->calory;
    if(this->harm){
        cout << ", is harmful";
    }
    else
    {
        cout << ", is harmless";
    }
}


