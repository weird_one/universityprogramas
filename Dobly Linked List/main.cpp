#include "List.h"

int main() {
    List myList;
    myList.addInHead(5);
    myList.addInHead(8);
    myList.addInHead(1);
    myList.addInHead(0);
    myList.addInHead(7);
    myList.addInHead(71);
    myList.addInHead(86);
    myList.addInHead(15);
    myList.sort();
    myList.display();
    return 0;
}
