
#include "Element.h"
#include "List.h"

void List::swap(Element *left, Element *right)
{
    int temp = left->val;
    left->val=right->val;
    right->val=temp;
}

void List::sort(Element *toSort)
{
    while(toSort->next != nullptr){
        if(toSort->val < toSort->next->val){
            swap(toSort,toSort->next);
        }
        toSort=toSort->next;
        sort(toSort);
    }
}

List::List()
{
    head = nullptr;
    tale = nullptr;
}

void List::addInHead(int val)
{
    Element *addBeg = new Element(val);
    if(head == nullptr) {
        addBeg->next = nullptr;
        addBeg->prev = nullptr;
        head = addBeg;
        tale = addBeg;
    }
    else {
        addBeg->prev = nullptr;
        head->prev = addBeg;
        addBeg->next = head;
        head = addBeg;
    }

}

void List::addInTale(int val)
{
    Element *addEnd = new Element(val);
    if(head == nullptr) {
        addEnd->next = nullptr;
        addEnd->prev = nullptr;
        head = addEnd;
        tale = addEnd;
    }
    else {
        addEnd->next = nullptr;
        tale->next = addEnd;
        addEnd->prev = tale;
        tale = addEnd;
    }
}

void List::sort()
{
    sort(head);
}

void List::display()
{
    Element *toDispaly = tale;
    cout << "List of data in Linked list" << endl;
    while(toDispaly != nullptr) {
        cout << toDispaly->val << endl;
        toDispaly = toDispaly->prev;
    }
}


