#ifndef DOBLY_LINKED_LIST_LIST_H
#define DOBLY_LINKED_LIST_LIST_H

#include "Element.h"
#include <iostream>

using namespace std;

class List {
private:
    Element *head;
    Element *tale;
    void swap (Element *left, Element *right);
    void sort(Element *toSort);

public:
    List();
    void addInHead(int val);
    void addInTale(int val);
    void sort();
    void display();
};


#endif //DOBLY_LINKED_LIST_LIST_H
