#ifndef DOBLY_LINKED_LIST_ELEMENT_H
#define DOBLY_LINKED_LIST_ELEMENT_H

class Element {
public:
    int val;
    Element* prev;
    Element* next;
    Element();
    Element(int val);
};


#endif //DOBLY_LINKED_LIST_ELEMENT_H
