cmake_minimum_required(VERSION 3.6)
project(AlgoritmWithTest)

set(CMAKE_CXX_STANDARD 11)

set(SOURCE_FILES main.cpp tests/test.cpp src/FastAlgorithm.cpp src/FastAlgorithm.h)
add_executable(AlgoritmWithTest ${SOURCE_FILES})

add_subdirectory(lib/googletest-master)
include_directories(lib/googletest-master/googletest/include)
include_directories(lib/googletest-master/googlemock/include)
target_link_libraries(AlgoritmWithTest gtest gtest_main)



