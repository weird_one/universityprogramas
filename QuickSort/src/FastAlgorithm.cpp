#include "FastAlgorithm.h"
#include <iostream>

using namespace std;

FastAlgorithm::FastAlgorithm() {}

void FastAlgorithm::quickSort(int* mass, int size)
{
    quickSort(mass, 0, size-1);
}

void FastAlgorithm::quickSort(int* mass, int startPoint, int endPoint)
{
    if(startPoint>=endPoint)
    {
        return;
    }

    int index = partition(mass, startPoint, endPoint);

    quickSort(mass, startPoint, index - 1);
    quickSort(mass, index, endPoint);

}

int FastAlgorithm::partition(int *mass, int startPoint, int endPoint) {
    int pivot = mass[endPoint];

    int smallIndex = startPoint;
    int currentIndex = startPoint;

    while(currentIndex < endPoint)
    {
        if(mass[currentIndex] < pivot)
        {
            swap(mass[smallIndex], mass[currentIndex]);
            smallIndex++;
        }
        currentIndex++;
    }

    swap(mass[smallIndex], mass[endPoint]);

    return smallIndex;
}
