#ifndef ALGORITMWITHTEST_FASTALGORITM_H
#define ALGORITMWITHTEST_FASTALGORITM_H


class FastAlgorithm {
public:
    FastAlgorithm();
    void quickSort(int* mass, int size);

private:
    void quickSort(int *mass, int startPoint, int endPoint);
    int partition(int *mass, int startPoint, int endPoint);
};


#endif //ALGORITMWITHTEST_FASTALGORITM_H
