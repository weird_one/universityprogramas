# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/wierdone/CLionProjects/AlgoritmWithTest/main.cpp" "/home/wierdone/CLionProjects/AlgoritmWithTest/cmake-build-debug/CMakeFiles/AlgoritmWithTest.dir/main.cpp.o"
  "/home/wierdone/CLionProjects/AlgoritmWithTest/src/FastAlgorithm.cpp" "/home/wierdone/CLionProjects/AlgoritmWithTest/cmake-build-debug/CMakeFiles/AlgoritmWithTest.dir/src/FastAlgorithm.cpp.o"
  "/home/wierdone/CLionProjects/AlgoritmWithTest/tests/test.cpp" "/home/wierdone/CLionProjects/AlgoritmWithTest/cmake-build-debug/CMakeFiles/AlgoritmWithTest.dir/tests/test.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "../lib/googletest-master/googletest/include"
  "../lib/googletest-master/googlemock/include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/wierdone/CLionProjects/AlgoritmWithTest/cmake-build-debug/lib/googletest-master/googlemock/gtest/CMakeFiles/gtest.dir/DependInfo.cmake"
  "/home/wierdone/CLionProjects/AlgoritmWithTest/cmake-build-debug/lib/googletest-master/googlemock/gtest/CMakeFiles/gtest_main.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
