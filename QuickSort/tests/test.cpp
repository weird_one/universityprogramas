#include <gtest/gtest.h>
#include "../src/FastAlgorithm.h"

using namespace std;

TEST(AlgoritmTest, SimpleTest)
{
    FastAlgorithm algoritm = FastAlgorithm();
    const int size = 2;
    int *mass = new int[size];
    mass[0] = 2;
    mass[1] = 1;
    algoritm.quickSort(mass, size);
    EXPECT_EQ(mass[0], 1);
    EXPECT_EQ(mass[1], 2);
}

TEST(AlgoritmTest, ZeroTest)
{
    FastAlgorithm algoritm = FastAlgorithm();
    const int size = 3;
    int *mass = new int[size];
    mass[0] = 0;
    mass[1] = 3;
    mass[2] = -1;
    algoritm.quickSort(mass, size);
    ASSERT_EQ(mass[0], -1);
    ASSERT_EQ(mass[1], 0);
    ASSERT_EQ(mass[2], 3);
}

TEST(AlgoritmTest, ManyTest)
{
    FastAlgorithm algoritm = FastAlgorithm();
    const int size = 10;
    int mass[size];
    mass[0] = 0;
    mass[1] = 3;
    mass[2] = -1;
    mass[3] = 6;
    mass[4] = 50;
    mass[5] = 2;
    mass[6] = 1;
    mass[7] = -16;
    mass[8] = -3;
    mass[9] = 4;
    algoritm.quickSort(mass, size);
    ASSERT_EQ(mass[0], -16);
    ASSERT_EQ(mass[1], -3);
    ASSERT_EQ(mass[2], -1);
    ASSERT_EQ(mass[3], 0);
    ASSERT_EQ(mass[4], 1);
    ASSERT_EQ(mass[5], 2);
    ASSERT_EQ(mass[6], 3);
    ASSERT_EQ(mass[7], 4);
    ASSERT_EQ(mass[8], 6);
    ASSERT_EQ(mass[9], 50);
}

