#include "Human.h"

Human::Human()
{
    name = "Lubomur";
    sername = "Byrui";
    age = 19;
}

Human::Human(const string &sername, const string &name, int age) :
        sername(sername),
        name(name),
        age(age)
{

}

ostream &operator<<(ostream &output, Human& human) {
    output << "Name is: "
           << human.name
           << ", surname is: "
           << human.sername
           << ", age is: "
           << human.age
           << endl;
    return output;
}

istream &operator>>(istream &input, Human& human) {
    cout << "Enter the name: ";
    input >> human.name;
    cout << "Enter the sername: ";
    input >> human.sername;
    cout << "Enter the age: ";
    input >> human.age;
    return input;
}

void Human::print(){
    cout << name << " ";
    cout << sername;
}

void Human::getThing(int thing){
    if(thing == age){
        cout << age;
    }
}

void Human::setSername(const string &sername) {
    Human::sername = sername;
}

void Human::setName(const string &name) {
    Human::name = name;
}

void Human::setAge(int age) {
    Human::age = age;
}

const string &Human::getSername() const {
    return sername;
}

const string &Human::getName() const {
    return name;
}
