#ifndef FOLLOWINGCLASS_HUMAN_H
#define FOLLOWINGCLASS_HUMAN_H

#include <iostream>

using namespace std;

class Human {
protected:
    string sername;
    string name;
    int age;
public:
    Human();
    Human(const string &sername, const string &name, int age);
    friend ostream& operator<<(ostream& output, Human &human);
    friend istream& operator>>(istream& input, Human &human);
    void getThing(int thing);
    void print();
    const string &getSername() const;
    const string &getName() const;
    void setSername(const string &sername);
    void setName(const string &name);
    void setAge(int age);
};


#endif //FOLLOWINGCLASS_HUMAN_H
