#ifndef FOLLOWINGCLASS_TEACHER_H
#define FOLLOWINGCLASS_TEACHER_H


#include "Human.h"

class Teacher : public Human
{
private:
    string chair;
    string position;
public:
    Teacher();
    Teacher(const string &sername, const string &name, int age, const string &chair, const string &position);
    friend ostream& operator<< (ostream& output, Teacher teacher);
    friend istream&operator>> (istream& input, Teacher& teacher);
    bool containNameAndSername(string name, string sername);
    void getThing(string thing);
    void print();
    void setChair(const string &chair);
    void setPosition(const string &position);
    const string &getChair() const;
};


#endif //FOLLOWINGCLASS_TEACHER_H
