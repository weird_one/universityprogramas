//
// Created by wierdone on 16.03.17.
//

#include "Teacher.h"

Teacher::Teacher()
{
    chair = "Mathemethics";
    position = "Student";
}

Teacher::Teacher(const string &sername, const string &name, int age, const string &chair, const string &position)
        : Human(sername, name, age),
          chair(chair),
          position(position)
{

}

ostream &operator<<(ostream &output, Teacher teacher)
{
    output << (Human&) teacher
           << "Chair is: "
           << teacher.chair
           << ", position is: "
           << teacher.position
           << endl;
    return output;
}

istream &operator>>(istream &input, Teacher& teacher) {
    input >> (Human&) teacher;
    cout << "Enter the chair: ";
    input >> teacher.chair;
    cout << "Enter the position: ";
    input >> teacher.position;
    return input;
}

bool Teacher::containNameAndSername(string name, string sername){
    return getName() == name && getSername() == sername;
}

void Teacher::setChair(const string &chair) {
    Teacher::chair = chair;
}

void Teacher::setPosition(const string &position) {
    Teacher::position = position;
}

const string &Teacher::getChair() const {
    return chair;
}

void Teacher::print() {
    Human::print();
    cout << chair << " ";
    cout << position;
}

void Teacher::getThing(string thing) {
    if(thing == position){
        cout << thing;
    }
}



