#include <iostream>
#include <fstream>
#include "Human/Human.h"
#include "Human/Teacher.h"
#include "Tools/FileReader.h"
#include "Tools/Sorter.h"
#include "Tools/Finder.h"
#include "Tools/FileWriter.h"

void print(Teacher* teachers, int size)
{
    for(int i = 0; i<size; i++){
        cout << teachers[i];
    }
}

int main() {
    Teacher* teachers;

    FileReader reader;
    reader.readFile();
    teachers = reader.teachers;
//
//    teachers[0].Human::print();
//    Human human("Любомир", "Бурий", 19);
//    human.print();
//    human.getThing(19);
//    teachers[0].getThing("Студент");
    Sorter sorter;
    sorter.sortByName(teachers, reader.size);

    print(teachers, reader.size);

    Finder finder;
    finder.find(teachers, reader.size);

    FileWriter writer;
    writer.writeToFile(teachers, reader.size);
    return 0;
}