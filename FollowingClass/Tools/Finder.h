//
// Created by wierdone on 23.03.17.
//

#ifndef FOLLOWINGCLASS_FINDER_H
#define FOLLOWINGCLASS_FINDER_H


#include "../Human/Teacher.h"

class Finder {
public:
    void find(Teacher *teachers, int size);
};


#endif //FOLLOWINGCLASS_FINDER_H
