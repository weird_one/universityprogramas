//
// Created by wierdone on 23.03.17.
//

#include "Sorter.h"

void Sorter::sortByName(Teacher *teachers, int size) {

        for(int i=0; i<size; i++)
        {
            for(int j=0; j<size-1-i; j++)
            {
                if(teachers[j].getName().compare(teachers[j+1].getName()) == 0)
                {
                    if(teachers[j].getSername().compare(teachers[j+1].getSername()) > 0)
                    {
                        swap(teachers[j], teachers[j + 1]);
                    }
                }
                else if(teachers[j].getName().compare(teachers[j+1].getName()) > 0)
                {
                    swap(teachers[j], teachers[j+1]);
                }
            }
        }
}
