//
// Created by wierdone on 23.03.17.
//

#include "Finder.h"

void Finder::find(Teacher *teachers, int size) {
    Teacher teachersTemp[size];
    int count = 0;
    string searchName;
    string searchSername;
    cout << "Введіть ім'я: ";
    cin >> searchName;
    cout << "Введіть прізвище: ";
    cin >> searchSername;
    for(int i=0; i<size; i++){
        if(teachers[i].containNameAndSername(searchName, searchSername)){
            teachersTemp[count] = teachers[i];
            count++;
        }
    }
    if(count == 0){
        cout << "Вибачте ви ввели неіснуючу точку маршруту." << endl;
    } else {
        for (int i = 0; i < count; i++) {
            cout << teachersTemp[i] << endl;
        }
    }

}
