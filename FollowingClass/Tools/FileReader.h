#ifndef CLASSFORUNIVERSITY_FILEREADER_H
#define CLASSFORUNIVERSITY_FILEREADER_H

#include "../Human/Teacher.h"

class FileReader {
public:
    FileReader();
    Teacher* teachers;
    int size;
    void readFile();
};


#endif //CLASSFORUNIVERSITY_FILEREADER_H
