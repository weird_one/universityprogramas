//
// Created by wierdone on 23.03.17.
//

#ifndef FOLLOWINGCLASS_SORTER_H
#define FOLLOWINGCLASS_SORTER_H


#include "../Human/Teacher.h"

class Sorter {
public:
    void sortByName(Teacher* teachers, int size);
};


#endif //FOLLOWINGCLASS_SORTER_H
