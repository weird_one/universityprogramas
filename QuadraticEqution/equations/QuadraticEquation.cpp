#include "QuadraticEquation.h"

void QuadraticEquation::equationRoots() {
    discriminant = pow(b,2) - 4*a*c;
    if(discriminant > 0)
    {
        result = (-b + sqrt(discriminant)) / 2*a;
        result2 = (-b - sqrt(discriminant)) / 2*a;
    }
    else
    {
        cout << "couldn`t find the roots" << endl;
        cout << endl;
    }
}

void QuadraticEquation::printRoots() {
    if(result != 0 && result2 != 0)
    {
        cout << "Equation is: " << a << "x^2 + " << "(" << b << ")" << "x + " << "(" << c << ")" << endl;
        cout << "Quadratic equation: " << "root 1: " << result << " root 2: " << result2 << endl;
        cout << endl;
    }
}

QuadraticEquation::QuadraticEquation(double a, double b, double c) :
        a(a),
        b(b),
        c(c)
{

}

double QuadraticEquation::getRoot() {
    return 0;
}

bool QuadraticEquation::isLinear() {
    return false;
}


