#ifndef QUADRATICEQUTION_EQUATION_H
#define QUADRATICEQUTION_EQUATION_H

#include <iostream>

using namespace std;

class Equation {
public:
    virtual void equationRoots() = 0;
    virtual void printRoots() = 0;
    virtual double getRoot() = 0;
    virtual bool isLinear() = 0;
};


#endif //QUADRATICEQUTION_EQUATION_H
