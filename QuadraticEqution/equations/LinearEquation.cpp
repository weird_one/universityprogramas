#include "LinearEquation.h"

void LinearEquation::equationRoots() {
    result = -(b/a);
}

void LinearEquation::printRoots() {
    cout << "Equation is :" << a << "x" << "+" << "(" << b << ") = 0" << endl;
    cout << "Linear equation root is: " << result << endl;
    cout << endl;
}


double LinearEquation::getRoot() {
    return result;
}

LinearEquation::LinearEquation(double a, double b) :
        a(a),
        b(b)
{

}

bool LinearEquation::isLinear() {
    return true;
}

