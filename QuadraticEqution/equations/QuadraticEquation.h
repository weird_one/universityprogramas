#ifndef QUADRATICEQUTION_QUADRATICEQUATION_H
#define QUADRATICEQUTION_QUADRATICEQUATION_H

#include <iostream>
#include "Equation.h"
#include "cmath"

using namespace std;

class QuadraticEquation : public Equation
{
private:
    double a;
    double b;
    double c;
    double result;
    double result2;
    double discriminant;
public:
    void equationRoots() override;
    QuadraticEquation(double a, double b, double c);

    bool isLinear() override;

    double getRoot() override;

    void printRoots() override;
};


#endif //QUADRATICEQUTION_QUADRATICEQUATION_H
