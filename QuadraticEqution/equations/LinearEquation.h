#ifndef QUADRATICEQUTION_LINEAREQUATION_H
#define QUADRATICEQUTION_LINEAREQUATION_H

#include "Equation.h"
#include <iostream>

using namespace std;

class LinearEquation : public Equation {
private:
    double result;
    double a;
    double b;
public:
    LinearEquation(double a, double b);
    bool isLinear() override;
    double getRoot() override;
    void equationRoots() override;
    void printRoots() override;
};


#endif //QUADRATICEQUTION_LINEAREQUATION_H
