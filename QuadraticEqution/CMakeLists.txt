cmake_minimum_required(VERSION 3.6)
project(QuadraticEqution)

set(CMAKE_CXX_STANDARD 11)

set(SOURCE_FILES main.cpp equations/Equation.cpp equations/Equation.h equations/LinearEquation.cpp equations/LinearEquation.h equations/QuadraticEquation.cpp equations/QuadraticEquation.h)
add_executable(QuadraticEqution ${SOURCE_FILES})