#include <iostream>
#include <fstream>
#include "equations/Equation.h"
#include "equations/LinearEquation.h"
#include "equations/QuadraticEquation.h"

using namespace std;

int globalSize;

Equation** readFromFile()
{
    double a;
    double b;
    double c;
    ifstream file("/home/wierdone/CLionProjects/QuadraticEqution/file.txt", ios::in);
    string line;
    int size;
    file >> size;
    Equation** equantions = new Equation*[size];
    globalSize = size;
    for (int i = 0; i < size; ++i)
    {
        file >> line;
        if(line == "linear")
        {
            file >> a;
            file >> b;
            equantions[i] = new LinearEquation(a, b);
        }
        else
        {
            file >> a;
            file >> b;
            file >> c;
            equantions[i] = new QuadraticEquation(a,b,c);
        }
    }
    return equantions;
}

int main() {
    Equation** equations = readFromFile();
    for (int i = 0; i < globalSize; ++i) {
        equations[i]->equationRoots();
        equations[i]->printRoots();
    }
    int a;
    int b;
    LinearEquation* temp;
    cout << "enter the borders: " << endl;
    cout << "a: ";
    cin >> a;
    cout << "b: ";
    cin >> b;
    for (int j = 0; j < globalSize; ++j) {
        temp = dynamic_cast<LinearEquation*> (equations[j]);
        if(temp != nullptr && temp->getRoot() > a && temp->getRoot() < b )
        {
            temp->printRoots();
        }
    }
    return 0;
}