#include "PriorityQueue.h"

PriorityQueue::PriorityQueue()
{
    front = nullptr;
}

void PriorityQueue::insert(int item, int priority)
{
    Element *toInsert;
    Element *toFindPriority;
    toInsert = new Element(priority, item);
    if (front == nullptr || priority > front->priority)
    {
        toInsert->link = front;
        front = toInsert;
    }
    else
    {
        toFindPriority = front;
        while (toFindPriority->link != nullptr && toFindPriority->link->priority >= priority)
        {
            toFindPriority=toFindPriority->link;
        }
        toInsert->link = toFindPriority->link;
        toFindPriority->link = toInsert;
    }
}

void PriorityQueue::pop()
{
    Element *toDelete;
    if(front == nullptr)
    {
        cout << "Queue is empty" << endl;
    }
    else
    {
        toDelete = front;
        cout << "Deleted item is: " << toDelete->info << endl;
        front = front->link;
        delete toDelete;
    }
}

int PriorityQueue::getWithHighestPriority() {
    if(front != nullptr)
    {
        return front->info;
    } 
    else
    {
        throw "Queue is empty";
    }
}

