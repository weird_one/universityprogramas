#ifndef TEST_PRIORITYQUEUE_H
#define TEST_PRIORITYQUEUE_H

#include <iostream>
#include "Element.h"

using namespace std;

class PriorityQueue {
private:
    Element *front;
public:
    PriorityQueue();
    void insert(int item, int priority);
    void pop();
    int getWithHighestPriority();
};


#endif //TEST_PRIORITYQUEUE_H
