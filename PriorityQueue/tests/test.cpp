#include <gtest/gtest.h>
#include "../PriorityQueue.h"

TEST(PriorityQueue, EmptyTest)
{
    PriorityQueue queue;
    ASSERT_ANY_THROW(queue.getWithHighestPriority());
}

TEST(PriorityQueue, PushTest)
{
    PriorityQueue queue;
    queue.insert(5, 5);
    queue.insert(3, 4);
    queue.insert(4, 3);
    queue.insert(6, 2);
    ASSERT_EQ(5, queue.getWithHighestPriority());
}

TEST(PriorityQueue, PopTest)
{
    PriorityQueue queue;
    queue.insert(5, 3);
    queue.insert(3, 1);
    queue.insert(4, 3);
    queue.insert(6, 2);
    queue.pop();
    ASSERT_EQ(4, queue.getWithHighestPriority());
}