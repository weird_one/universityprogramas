//
// Created by Roger on 4/24/2017.
//

#ifndef TEST_ELEMENT_H
#define TEST_ELEMENT_H


class Element {
public:
    int priority;
    int info;
    Element *link;
    Element(int priority, int info);
};


#endif //TEST_ELEMENT_H
