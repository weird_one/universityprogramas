#ifndef CLASSFORUNIVERSITY_INTERFACE_H
#define CLASSFORUNIVERSITY_INTERFACE_H


#include <tools/FileReader.h>
#include <tools/Finder.h>
#include <tools/Sorter.h>

class Interface {
public:
    Interface();
private:
    int size;
    Building *path;
    void showStartScreen();
    void showMenu();
    void showThePaths();
    void sortByCondition();
    void findThePath();
    void getBackToTheMenu();

    void enterThePath();
};


#endif //CLASSFORUNIVERSITY_INTERFACE_H
