#include "Interface.h"

Interface::Interface() {
    FileReader reader = FileReader();
    this->path = reader.building;
    this->size = reader.size;
    showStartScreen();
}

void Interface::showThePaths(){
    cout << string(3, '\n');
    for(int i=0; i<size; i++){
        cout << path[i] << endl;
    }
    cout << endl;
    cout << "Назад [Enter] ";
    getBackToTheMenu();
}

void Interface::showMenu() {
    int menuCase;
    cout << string(3, '\n');
    cout << "Меню:" << endl;
    cout << "Показаити будинки [1]" << endl;
    cout << "Додати маршрут в базу [2]" << endl;
    cout << "Посортувати маршрути за певним критерієм [3]" << endl;
    cout << "Знайти маршрут за кінцевими точками[4]" << endl;\
    cout << "Завершити роботу[5]" << endl;
    cout << "Ви бажаєте: ";
    cin >> menuCase;
    switch (menuCase){
        case 1: showThePaths();
            break;
        case 3: sortByCondition();
            break;
        case 4: findThePath();
            break;
        case 5: exit(EXIT_SUCCESS);
        case 2: enterThePath();
        default: cout << "Виберіть будь-ласка існуючий пункт меню." << endl;
                    showMenu();
    }
}

void Interface::showStartScreen() {
    cout << "Вітаю в тестовій програму класу МАРШРУТ" << endl;
    cout << "Бажаєте почати роботу? [Enter]";
    getBackToTheMenu();
}

void Interface::sortByCondition() {
    Sorter sorter = Sorter(path, size);
    cout << string(3, '\n');
    cout << "Ви можете посортувати маршрути за наступними критеріями: " << endl;
    cout << "За назв (по алфавіту) [1]" << endl;
    cout << "За адр (по алфавіту) [2]" << endl;
    cout << "За пл (по зростанню) [3]" << endl;
    cout << "За пл (по спаданню) [4]" << endl;
    cout << "Повернутись в меню [5]" << endl;
    int menuCase;
    cout << "Ви бажаєте: ";
    cin >> menuCase;
    cout << string(3, '\n');
    switch (menuCase){
        case 1:
            sorter.sortBySquraeOfBuiding();
            break;
        case 2:
            sorter.sortByNumberOfBuiding();
            break;
        case 3:
            sorter.sortBySquareIncrease();
            break;
        case 4:
            sorter.sortByNumberDecrease();
            break;
        case 5:
            showMenu();
        default: cout << "Виберіть будь-ласка існуючий пункт меню." << endl;
                    showMenu();
    }
    cout << "Назад [Enter] ";
    cin.get();
    if(cin.get() == '\n'){
        sortByCondition();
    } else{
        cout << "До побачення" << endl;
    }
}

void Interface::findThePath() {
    Finder finder = Finder(path, size);
    cout << string(3, '\n');
    finder.find();
    cout << endl;
    cout << "Знайти інший маршрут [1]" << endl;
    cout << "Повернутись в меню [2]" << endl;
    int menuCase;
    cout << "Ви бажаєте: ";
    cin >> menuCase;
    switch (menuCase) {
        case 1:
            findThePath();
            break;
        case 2:
            showMenu();
        default:
            cout << "Виберіть будь-ласка існуючий пункт меню." << endl;
                    showMenu();
    }
}

void Interface::getBackToTheMenu() {
    cin.get();
    if(cin.get() == '\n'){
        showMenu();
    } else{
        cout << "До побачення" << endl;
    }
}

void Interface::enterThePath() {
    cout << string(3, '\n');
    cout << "Введіть новий маршрут:" << endl;
    Building pathInput;
    cin >> pathInput;
    cout << string(1, '\n');
    cout << "Ви ввели наступний маршрут: " << endl;
    cout << pathInput << endl;

    cout << string(1, '\n');
    cout << "Ввести ще один маршрут [1]" << endl;
    cout << "Повернутись в меню [2]" << endl;
    int menuCase;
    cin >> menuCase;
    switch (menuCase){
        case 1: enterThePath();
            break;
        case 2: showMenu();
            break;
        default: cout << "Виберіть будь-ласка існуючий пункт меню." << endl;
                    showMenu();
    }
}


