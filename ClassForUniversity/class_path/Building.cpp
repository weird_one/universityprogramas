#include <algorithm>
#include "Building.h"

Building::Building(const string &streetName, const int &number, double square) :
        streetName(streetName),
        number(number),
        square(square) {}

Building::Building() {}

ostream &operator<<(ostream &output, const Building &P) {
    output << "Початкова точка: " << P.streetName
           << ", \tкінцева точка: " << P.number
           << ", \tдовжина маршруту: " << P.square;
    return output;
}

istream &operator>>(istream &input, Building &P) {
    cout << "Введіть початкову точку: ";
    input >> P.streetName;
    cout << "Введіть кінцеву точку: ";
    input >> P.number;
    cout << "Введіть довжину: ";
    input >> P.square;
    return input;
}

Building& Building::operator=(Building p) {
    streetName = p.streetName;
    number = p.number;
    square = p.square;
    return *this;
}

bool Building::contain(string streetName) {
    return streetName == streetName;
}
