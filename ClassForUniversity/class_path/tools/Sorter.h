#ifndef CLASSFORUNIVERSITY_SORTER_H
#define CLASSFORUNIVERSITY_SORTER_H


#include "Building.h"

class Sorter {
public:
    Building *building;
    int size;
    Sorter(Building *path, int size);
    void sortBySquraeOfBuiding();
    void sortByNumberOfBuiding();
    void sortBySquareIncrease();
    void sortByNumberDecrease();
};


#endif //CLASSFORUNIVERSITY_SORTER_H
