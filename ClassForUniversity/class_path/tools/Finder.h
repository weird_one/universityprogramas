#ifndef CLASSFORUNIVERSITY_FINDER_H
#define CLASSFORUNIVERSITY_FINDER_H


#include <Building.h>

class Finder {
public:
    Building *building;
    int size;
    Finder(Building *building, int size);
    void find();
};


#endif //CLASSFORUNIVERSITY_FINDER_H
