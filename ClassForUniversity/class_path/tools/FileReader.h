#ifndef CLASSFORUNIVERSITY_FILEREADER_H
#define CLASSFORUNIVERSITY_FILEREADER_H


#include <Building.h>

class FileReader {
public:
    FileReader();
    Building* building;
    int size;
private:
    void readFile();
};


#endif //CLASSFORUNIVERSITY_FILEREADER_H
