#include "Finder.h"

Finder::Finder(Building *building, int size) : building(building), size(size) {}

void Finder::find() {
    Building buildTemp[size];
    int count = 0;
    string searchPhrase;
    cout << "Enter the name of street: ";
    cin >> searchPhrase;
    for(int i=0; i<size; i++){
        if(building[i].contain(searchPhrase)){
            buildTemp[count] = building[i];
            count++;
        }
    }
    if(count == 0){
        cout << "No such adress." << endl;
    }
    else
    {
        for (int i = 0; i < count; i++)
        {
            cout << buildTemp[i] << endl;
        }
    }
}
