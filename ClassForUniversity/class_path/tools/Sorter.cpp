#include "Sorter.h"


Sorter::Sorter(Building *path, int size) : building(path), size(size) {}

void Sorter::sortBySquareIncrease() {
    Building temp;
    for(int i=0; i<size; i++){
        for(int j=0; j<size-1-i; j++){
            if(building[j].square < building[j+1].square){
                temp = building[j];
                building[j] = building[j+1];
                building[j+1] = temp;
            }
        }
    }

    for(int i=0; i<size; i++){
        cout << building[i] << endl;
    }
}

void Sorter::sortByNumberOfBuiding() {
    Building temp;
    for(int i=0; i<size; i++){
        for(int j=0; j<size-1-i; j++){
            if(building[j].number > building[j+1].number){
                temp = building[j];
                building[j] = building[j+1];
                building[j+1] = temp;
            }
        }
    }

    for(int i=0; i<size; i++){
        cout << building[i] << endl;
    }
}

void Sorter::sortBySquraeOfBuiding() {
    Building temp;
    for(int i=0; i<size; i++){
        for(int j=0; j<size-1-i; j++){
            if(building[j].streetName > building[j+1].streetName){
                temp = building[j];
                building[j] = building[j+1];
                building[j+1] = temp;
            }
        }
    }
    for(int i=0; i<size; i++){
        cout << building[i] << endl;
    }
}


void Sorter::sortByNumberDecrease() {
    Building temp;
    for(int i=0; i<size; i++){
        for(int j=0; j<size-1-i; j++){
            if(building[j].square < building[j+1].square){
                temp = building[j];
                building[j] = building[j+1];
                building[j+1] = temp;
            }
        }
    }

    for(int i=0; i<size; i++){
        cout << building[i] << endl;
    }
}
