#include <fstream>
#include "FileReader.h"

void FileReader::readFile() {
    ifstream myFile("/home/wierdone/CLionProjects/ClassForUniversity/example.txt", ios::in);
    myFile >> size;
    building = new Building[size];
    string line;
    int number;
    double length;
    if(!myFile.is_open()){
        cout << "Cant open the file" << endl;
    }
    else{
        for(int i=0; i<size; i++){
            myFile >> line;
            building[i].streetName = line;
            myFile >> number;
            building[i].number = number;
            myFile >> length;
            building[i].square = length;
        }
    }
}

FileReader::FileReader() {
    readFile();
}
