#ifndef CLASSFORUNIVERSITY_PATH_H
#define CLASSFORUNIVERSITY_PATH_H

#include <iostream>
using namespace std;

class Building {
public:
    string streetName;
    int number;
    double square;
    Building();
    Building(const string &streetName, const int &number, double square);
    bool contain(string streetName);
    Building& operator=(Building p);
    friend ostream &operator<<(ostream &output, const Building &P);
    friend istream &operator>>(istream &input, Building &P);
};

#endif //CLASSFORUNIVERSITY_PATH_H
