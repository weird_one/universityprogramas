# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/wierdone/CLionProjects/ClassForUniversity/testClass/tests/test.cpp" "/home/wierdone/CLionProjects/ClassForUniversity/cmake-build-debug/testClass/tests/CMakeFiles/runClassTests.dir/test.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "../classPath"
  "../lib/googletest-master/googletest/include"
  "../lib/googletest-master/googletest"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/wierdone/CLionProjects/ClassForUniversity/cmake-build-debug/lib/googletest-master/googlemock/gtest/CMakeFiles/gtest.dir/DependInfo.cmake"
  "/home/wierdone/CLionProjects/ClassForUniversity/cmake-build-debug/lib/googletest-master/googlemock/gtest/CMakeFiles/gtest_main.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
