#ifndef SIMPLETABLE_ELEMENT_H
#define SIMPLETABLE_ELEMENT_H

#include <iostream>

using namespace std;

class Element
{
public:
    string key;
    string val;
    Element* next;
    Element(const string &key, const string &val);
};


#endif //SIMPLETABLE_ELEMENT_H
