#include "gtest/gtest.h"
#include "Table.h"


void doThing(Element *element)
{
    element->val += " Hello";
}

TEST(TableTests, AddTest)
{
    Table table;
    table.add("Key", "Monkey");
    table.add("Yek", "Tiger");
    table.add("Bird", "Falcon");
    ASSERT_EQ("Falcon", table.getValueByKey("Bird"));
    ASSERT_EQ("Tiger", table.getValueByKey("Yek"));
}

TEST(TableTests, ReplaceTest)
{
    Table table;
    table.add("Key", "Monkey");
    table.add("Bird", "Hawk");
    table.replaceByKey("Bird", "Falcon");
    ASSERT_EQ("Falcon", table.getValueByKey("Bird"));
}

TEST(TableTests, SearchTest)
{
    Table table;
    table.add("Key", "Monkey");
    table.add("Yek", "Tiger");
    table.add("Bird", "Falcon");
    table.add("Koo", "Moo");
    string elementVal;
    elementVal = table.search("Bird").val;
    ASSERT_EQ("Falcon", elementVal);
}

TEST(TableTests, IterateTest)
{
    Table table;
    table.add("Key", "Monkey");
    table.add("Yek", "Tiger");
    table.add("Bird", "Falcon");
    table.add("Koo", "Moo");
    table.iterate(doThing);
    ASSERT_EQ("Moo Hello", table.getValueByKey("Koo"));
}