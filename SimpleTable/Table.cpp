#include "Table.h"

Table::Table()
{
    head = nullptr;
}

void Table::add(string key, string val)
{
    Element* elementToAdd = head;
    if(head == nullptr)
    {
        head = new Element(key, val);
    }
    else
    {
        while (elementToAdd->next != nullptr)
        {
            elementToAdd = elementToAdd->next;
        }
        elementToAdd->next = new Element(key, val);
    }
}

void Table::deleteElement(string key)
{
    Element* elementPrevious;
    Element* elementNext;
    Element* elementToDelete = head;
    if(head->key == key)
    {
        if(head->next == nullptr)
        {
            head = new Element("Empty", "Empty");
        }
        else {
            head = head->next;
        }
    }
    else
    {
        while (elementToDelete->next->key != key)
        {
            elementToDelete = elementToDelete->next;
        }

        elementPrevious = elementToDelete;
        elementToDelete = elementToDelete->next;
        elementNext = elementToDelete->next;

        delete elementToDelete;
        elementPrevious->next = elementNext;
    }
}

void Table::replaceByKey(string key, string val)
{
    Element* elementToReplace = head;
    while(elementToReplace->key != key)
    {
        elementToReplace = elementToReplace->next;
    }
    elementToReplace->val = val;
}

Element Table::search(string key)
{
    Element* findElement = head;
    while(findElement->key != key)
    {
        findElement = findElement->next;
    }
    return *findElement;
}



void Table::iterate(void (*doThing)(Element*))
{
    void (*func)(Element*);
    func = doThing;
    Element* elementToIterate = head;
    while(elementToIterate != nullptr)
    {
        func(elementToIterate);
        elementToIterate = elementToIterate->next;
    }
}

string Table::getValueByKey(string key)
{
    if (head != nullptr)
    {
        Element *findElement = head;
        while (findElement->key != key)
        {
            findElement = findElement->next;
        }
        return findElement->val;
    }
    else
    {
        return "Your table is empty";
    }
}

