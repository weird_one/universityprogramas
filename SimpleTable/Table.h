#ifndef SIMPLETABLE_TABLE_H
#define SIMPLETABLE_TABLE_H

#include <iostream>
#include <functional>
#include "Element.h"

using namespace std;

class Table
{
private:
    Element* head;
public:
    Table();
    void add(string key, string val);
    void deleteElement(string key);
    void replaceByKey(string key, string val);
    Element search(string key);
    void iterate(void (*doThing)(Element*));
    string getValueByKey(string key);
};


#endif //SIMPLETABLE_TABLE_H
