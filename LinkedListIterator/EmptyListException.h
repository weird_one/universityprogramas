#ifndef LINKEDLISTITERATOR_EMPTYLISTEXCEPTION_H
#define LINKEDLISTITERATOR_EMPTYLISTEXCEPTION_H

#include <iostream>

using namespace std;

class EmptyListException : exception{
private:
    string functionCall;
public:
    explicit EmptyListException(const string &functionCall) : functionCall(functionCall) {}
    const char *what() const noexcept override{
        string massage = "You try to " + functionCall + " element, but your list is empty. \nTry to add some elements.";
        return massage.c_str();
    }
};


#endif //LINKEDLISTITERATOR_EMPTYLISTEXCEPTION_H
