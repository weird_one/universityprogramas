#ifndef LINKEDLISTITERATOR_LINKEDLIST_H
#define LINKEDLISTITERATOR_LINKEDLIST_H

#include <iostream>
#include <functional>
#include "Element.h"
#include "EmptyListException.h"
#include "EndOfListException.h"

using namespace std;

template <typename Val>
class LinkedList {
private:
    Element<Val>* head;
public:
    class Iterator;
    friend class Iterator;

    LinkedList()
    {
        head = nullptr;
    }

    void add(Val val)
    {
        Element<Val>* elementToAdd = head;
        if(head == nullptr)
        {
            head = new Element<Val>(val);
        }
        else
        {
            while (elementToAdd->next != nullptr)
            {
                elementToAdd = elementToAdd->next;
            }
            elementToAdd->next = new Element<Val>(val);
        }
    }

    void deleteElement(Val val)
    {
        if(head == nullptr)
        {
            throw EmptyListException("delete");
        }

        Element<Val>* elementToDelete = head;
        if(head->val == val)
        {
            head = head->next;
            delete elementToDelete;
        }
        else
        {
            Element<Val>* elementPrevious;
            Element<Val>* elementNext;
            while (elementToDelete->next->val != val)
            {
                elementToDelete = elementToDelete->next;
            }
            elementPrevious = elementToDelete;
            elementToDelete = elementToDelete->next;
            elementNext = elementToDelete->next;
            delete elementToDelete;
            elementPrevious->next = elementNext;
        }
    }

    void printList(){
        if(head == nullptr)
        {
            throw EmptyListException("print");
        }
        Element<Val>* toPrint = head;
        while(toPrint != nullptr)
        {
            cout << toPrint->val << " ";
            toPrint = toPrint->next;
        }
    };

    void replace(Val oldVal, Val newVal)
    {
        Element<Val>* elementToReplace = head;
        while(elementToReplace->val != oldVal)
        {
            elementToReplace = elementToReplace->next;
        }
        elementToReplace->val = newVal;
    }

    Iterator beginIterate()
    {
        return Iterator(head);
    }

    void refreshIterator(Iterator& iterator)
    {
        iterator = Iterator(head);
    }

    class Iterator{
    protected:
        Element<Val>* currentElement;
    public:
        explicit Iterator(Element<Val> *currentElement) : currentElement(currentElement) {}

        Iterator& operator++()
        {
            if(currentElement == nullptr)
            {
                throw EndOfListException();
            }
            currentElement = currentElement->next;
            return  *this;
        }

        Iterator operator++(int)
        {
            Iterator temp = *this;
            operator++();
            return  temp;
        }

        Val operator*()
        {
            if(!currentElement)
            {
                throw EndOfListException();
            }
            return currentElement->val;
        }

        bool operator ==(Iterator& iterator)
        {
            return currentElement == iterator.currentElement;
        }

        bool operator != (Iterator& iterator)
        {
            return currentElement != iterator.currentElement;
        }

    };
};


#endif //LINKEDLISTITERATOR_LINKEDLIST_H
