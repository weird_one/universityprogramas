#ifndef LINKEDLISTITERATOR_ENDOFLISTEXCEPTION_H
#define LINKEDLISTITERATOR_ENDOFLISTEXCEPTION_H

#include <iostream>

using namespace std;

class EndOfListException : exception{
public:
    const char *what() const noexcept override{
        return "You reach to the end of the list. Use List::refrashIterator(Iterator)";
    }
};


#endif //LINKEDLISTITERATOR_ENDOFLISTEXCEPTION_H
