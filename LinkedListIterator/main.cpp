#include <iostream>
#include "LinkedList.h"

int main() {
    try {
        LinkedList<int> list;
        list.add(3);
        list.add(6);
        list.add(1);
        list.add(9);
        LinkedList<int>::Iterator iterator = list.beginIterate();
    }
    catch (EmptyListException& e)
    {
        cout << e.what();
    }
    catch (EndOfListException& e)
    {
        cout << e.what();
    }
    return 0;
}