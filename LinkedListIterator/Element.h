#ifndef LINKEDLISTITERATOR_ELEMENT_H
#define LINKEDLISTITERATOR_ELEMENT_H

#include <iostream>

using namespace std;

template <typename Val>
class Element
{
public:
    Val val;
    Element* next;

    explicit Element(Val& val) : val(val)
    {
        next = nullptr;
    }

};
#endif //LINKEDLISTITERATOR_ELEMENT_H
