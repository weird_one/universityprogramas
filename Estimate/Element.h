#ifndef ESTIMATE_ELEMENT_H
#define ESTIMATE_ELEMENT_H

template <class Val>

class Element{
public:
    Val val;
    Element* next;
    Element() {}

    Element(Val val, Element *next) : val(val), next(next) {}
};

#endif //ESTIMATE_ELEMENT_H
