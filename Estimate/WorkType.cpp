#include "WorkType.h"

ostream &operator<<(ostream &output, WorkType& work)
{
    output << "назва: "
         << work.name
         << ", вартість: "
         << work.price
         << ", обсяг(дн): "
         << work.volume
         << endl;
    return output;
}

istream &operator>>(istream &input, WorkType& work) {
    input >> work.name;
    input >> work.price;
    input >> work.volume;
    return input;
}

bool WorkType::operator<(WorkType &work)
{
    return name < work.name;
}

bool WorkType::operator>(WorkType &work) {
    return name > work.name;
}

double WorkType::getPrice() const {
    return price;
}

