#include <iostream>
#include <fstream>
#include "LinkedList.h"
#include "WorkType.h"

using namespace std;

int globalSize;

LinkedList<WorkType> readFromFile()
{
    ifstream file("../input.txt", ios::in);
    LinkedList<WorkType> container;
    WorkType toInput;
    int size;
    file >> size;
    globalSize = size;
    for(int i=0; i<size; i++){
        file >> toInput;
        container.add(toInput);
    }
    file.close();
    return container;
}

void sortByName(LinkedList<WorkType> list)
{
    for(int i=0; i<globalSize; i++){
        for(int j=0; j<globalSize-1-i; j++){
            if(list[j] > list[j+1]){
                list.swap(j);
            }
        }
    }
}

void writeToFile(LinkedList<WorkType> list)
{
    ofstream file("../output.txt", ios::out);
    for(int i = 0; i<globalSize; i++)
    {
        file << list[i];
    }
}

void getTheSumm(LinkedList<WorkType> list)
{
    int price = 0;
    for(int i=0; i<globalSize; i++)
    {
        price += list[i].getPrice();
    }

    cout << "Сумма робіт: " << price << endl;
}

int main() {
    LinkedList<WorkType> list = readFromFile();
    sortByName(list);
    writeToFile(list);
    getTheSumm(list);

    return 0;
}