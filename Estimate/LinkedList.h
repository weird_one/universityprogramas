#ifndef ESTIMATE_LINKEDLIST_H
#define ESTIMATE_LINKEDLIST_H

#include "Element.h"
#include "ArrayOutOfBoundExeption.h"

using namespace std;

template <class Val>
class LinkedList
{
private:
    Element<Val>* head;
public:
    LinkedList()
    {
        head = nullptr;
    }

    void add(Val val){
        if(head == nullptr)
        {
            head = new Element<Val>(val, nullptr);
        }
        else
        {
            Element<Val>* toAdd = head;
            while(toAdd->next != nullptr)
            {
                toAdd = toAdd->next;
            }
            toAdd->next = new Element<Val>(val, nullptr);
        }
    }

    void printList(){
        Element<Val>* toPrint;
        toPrint = head;
        if(head == nullptr)
        {
            cout << "List is empty" << endl;
        }
        while(toPrint != nullptr)
        {
            cout << toPrint->val;
            toPrint = toPrint->next;
        }
    };

    Val& operator[](int index)
    {
        Element<Val>* toGet;
        toGet = head;
        if(head == nullptr)
        {
            throw ArrayOutOfBoundExeption();
        }
        for(int i=0; i<index; i++){
            if(toGet == nullptr)
            {
                throw ArrayOutOfBoundExeption();
            }
            toGet = toGet->next;
        }
        return toGet->val;
    }

    void swap(int index)
    {
        Element<Val>* toSwap = head;
        Val temp;
        if(head == nullptr)
        {
            throw ArrayOutOfBoundExeption();
        }
        for(int i=0; i<index; i++)
        {
            toSwap = toSwap->next;
        }
        if(toSwap->next == nullptr)
        {
            throw ArrayOutOfBoundExeption();
        }
        temp = toSwap->next->val;
        toSwap->next->val = toSwap->val;
        toSwap->val = temp;
    }
};



#endif //ESTIMATE_LINKEDLIST_H
