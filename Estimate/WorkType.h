#ifndef ESTIMATE_WORKTYPE_H
#define ESTIMATE_WORKTYPE_H

#include <iostream>

using namespace std;

class WorkType {
private:
    string name;
    double price;
    double volume;
public:
    friend ostream&operator <<(ostream& output, WorkType& work);
    friend istream&operator >> (istream& input, WorkType& work);
    bool operator<(WorkType &work);
    bool operator>(WorkType &work);

    double getPrice() const;
};


#endif //ESTIMATE_WORKTYPE_H
