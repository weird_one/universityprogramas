#ifndef QUIZ_QUIZMAKER_H
#define QUIZ_QUIZMAKER_H


#include "Quiz.h"

class QuizMaker {
private:
    Quiz* head;
    int mark;
    void modify(Quiz* toModify);
public:
    QuizMaker();
    void add(Quiz* val);
    void addFromInput();
    void deleteQuestion(int number);
    void modifyQuestion(int number);
    void doQuiz();
    void printResult();
    void print();
};


#endif //QUIZ_QUIZMAKER_H
