#include <fstream>
#include <limits>
#include "FileReader.h"
#include "WrongTypeExeption.h"
#include "FileReaderExeption.h"

FileReader::FileReader()
{

}

void FileReader::read()
{
    ifstream file("/home/wierdone/CLionProjects/Quiz/questions.txt", ios::in);
    string answers[5];
    string question;
    int mark;
    int rightAnswer;
    if(!file.is_open())
    {
        throw FileReaderExeption();
    }
    else
    {
        while(file.good())
        {
            if (!getline(file, question, '\r'))
            {
                throw WrongTypeExeption("питання");
            }
            for(int j=0; j<5; j++)
            {
                if(!(file >> answers[j]))
                {
                    throw WrongTypeExeption("відповіді");
                }
            }
            if (!(file >> mark))
            {
                throw WrongTypeExeption("оцінку");
            }
            if (!(file >> rightAnswer))
            {
                throw WrongTypeExeption("відповідь");
            }
            questionList.add(new Quiz(question, answers, mark, rightAnswer));
            file.ignore(numeric_limits<streamsize>::max(), '\n');
        }
    }
}
