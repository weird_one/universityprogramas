#include <iostream>
#include "FileReader.h"
#include "FileReaderExeption.h"
#include "WrongTypeExeption.h"
#include "WrongInputExeption.h"

QuizMaker list;
bool wantDo;

void showMenu()
{
    int number = 0;
    cout << "Меню: " << endl;
    cout << "Подивитись список питань [1]" << endl;
    cout << "Додати питання [2]" << endl;
    cout << "Видалити питання [3]" << endl;
    cout << "Модифікувати питання [4]" << endl;
    cout << "Пройти тест [5]" << endl;
    int choise = 0;
    cout << " - ";
    cin >> choise;

    switch(choise)
    {
        case 1:
            list.print();
            break;
        case 2:
            list.addFromInput();
            break;
        case 3:
            cout << endl;
            cout << "Яке питання ви хочете видалити: " << endl;
            cin >> number;
            list.deleteQuestion(number);
            break;
        case 4:
            cout << endl;
            cout << "Яке питання ви хочете модифікувати: " << endl;
            int number2;
            cin >> number2;
            list.modifyQuestion(number2);
            break;
        case 5:
            list.doQuiz();
            list.printResult();
            break;
        default:
            cout << "Введіть правильний варіант" << endl;
    }

    cout << "Бажаєте ще щось зробити? [0/1]" << endl;
    int wantToDo;
    cin >> wantToDo;
    if(wantToDo == 0 || wantToDo == 1)
    {
        if(wantToDo == 1)
        {
            showMenu();
        }
        if(wantToDo == 0)
        {
            wantDo = false;
            return;
        }
    }
    else
    {
        throw WrongInputExeption();
    }
}

int main() {
    try
    {
        FileReader reader;
        reader.read();
        list = reader.questionList;
    }
    catch (FileReaderExeption e)
    {
        e.printError();
    }
    catch  (WrongTypeExeption e)
    {
        e.print();
    }

    wantDo = true;

    while (wantDo)
    {
        try {
            showMenu();
        }
        catch (WrongInputExeption e)
        {
            e.print();
        }
    }


    return 0;
}