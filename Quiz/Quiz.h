#ifndef QUIZ_QUIZ_H
#define QUIZ_QUIZ_H

#include <iostream>

using namespace std;

class Quiz {
private:
    string question;
    string answers[5];
    int rightAnswer;
    int mark;

public:
    Quiz* next;
    Quiz();
    Quiz(string question, string answers[5], int rightAnswer, int mark);
    void printQuestion();
    void printAnswers();

    void setQuestion(const string &question);
    void setAnswers(string answers[5]);
    void setRightAnswer(int rightAnswer);

    void setMark(int mark);

    int getMark() const;

    int getRightAnswer() const;
};


#endif //QUIZ_QUIZ_H
