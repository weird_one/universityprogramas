//
// Created by wierdone on 23.05.17.
//

#ifndef MODULE2_WRONGTYPEEXEPTION_H
#define MODULE2_WRONGTYPEEXEPTION_H

#include <iostream>

using namespace std;

class WrongTypeExeption {
private:
    string caseOfError;
public:
    WrongTypeExeption(const string &caseOfError);
    void print();
};


#endif //MODULE2_WRONGTYPEEXEPTION_H
