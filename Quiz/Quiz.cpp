#include "Quiz.h"

Quiz::Quiz(string question, string answersFromInput[5], int rightAnswer, int mark) :
        rightAnswer(rightAnswer),
        mark(mark),
        question(question)

{
    for(int i=0; i<5; i++)
    {
        answers[i] = answersFromInput[i];
    }
    next = nullptr;
}

Quiz::Quiz()
{
    next = nullptr;
}

void Quiz::printQuestion()
{
   cout << " - " << question << endl;
}

void Quiz::printAnswers()
{
    for(int i=0; i<5; i++)
    {
        cout << i+1 <<". " << answers[i] << endl;
    }

}

int Quiz::getRightAnswer() const {
    return rightAnswer;
}

int Quiz::getMark() const {
    return mark;
}

void Quiz::setQuestion(const string &question) {
    Quiz::question = question;
}

void Quiz::setRightAnswer(int rightAnswer) {
    Quiz::rightAnswer = rightAnswer;
}

void Quiz::setMark(int mark) {
    Quiz::mark = mark;
}

void Quiz::setAnswers(string *answersFromInput)
{
    for(int i=0; i<5; i++)
    {
        answers[i] = answersFromInput[i];
    }
}
