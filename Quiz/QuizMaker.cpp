
#include "QuizMaker.h"

QuizMaker::QuizMaker()
{
    head = nullptr;
    mark = 0;
}

void QuizMaker::add(Quiz *val)
{
    if(head == nullptr)
    {
        head = val;
    }
    else
    {
        Quiz* toAdd = head;
        while(toAdd->next != nullptr)
        {
            toAdd = toAdd->next;
        }
        toAdd->next = val;
    }
}

void QuizMaker::print()
{
    cout << "Ваш список питань: " << endl;
    Quiz* toShow = head;
    while(toShow != nullptr)
    {
        toShow->printQuestion();
        toShow = toShow->next;
    }
}

void QuizMaker::doQuiz()
{
    cout << "Розпочнемо тест!" << endl;
    Quiz* toAsk = head;
    int count = 1;
    while (toAsk != nullptr)
    {
        int answer;
        cout << " - питання № " << count << endl;
        count++;
        toAsk->printQuestion();
        toAsk->printAnswers();
        cout << "відповідь: ";
        cin >> answer;
        if(answer == toAsk->getRightAnswer())
        {
            mark += toAsk->getMark();
        }
        toAsk = toAsk->next;
        cout << endl;
    }
}

void QuizMaker::printResult()
{
    cout << "Ви набрали " << mark << " балів." << endl;
}

void QuizMaker::addFromInput()
{
    string question;
    string answres[5];
    int rightAnswer;
    int mark;
    cout << "питання: ";
    cin.ignore();
    getline(cin, question);
    cout << "відповіді ";
    for(int i=0; i<5; i++)
    {
        cout << i << ": ";
        cin >> answres[i];
    }
    cout <<"№ відповіді: ";
    cin >> rightAnswer;
    cout << "к-сть балів за питання: ";
    cin >> mark;
    add(new Quiz(question, answres, rightAnswer, mark));
}

void QuizMaker::deleteQuestion(int number)
{
    if(head == nullptr)
    {
        return;
    }
    if(number == 1)
    {
        Quiz* temp;
        temp = head->next;
        delete head;
        head = temp;
        return;
    }
    Quiz* toDelete = head;
    int count = 0;
    while(toDelete != nullptr && count < number-2)
    {
        toDelete = toDelete->next;
        count++;
    }
    if(count == number-2)
    {
        if(toDelete->next == nullptr)
        {
            return;
        }
        Quiz* temp = toDelete->next->next;
        delete toDelete->next;
        toDelete->next = temp;
    }
}

void QuizMaker::modifyQuestion(int number)
{
    if(head == nullptr)
    {
        return;
    }
    if(number == 1)
    {
        modify(head);
    }
    Quiz* toDelete = head;
    int count = 0;
    while(toDelete != nullptr && count < number-2)
    {
        toDelete = toDelete->next;
        count++;
    }
    if(count == number-2)
    {
        if(toDelete->next == nullptr)
        {
            return;
        }
        modify(toDelete->next);
    }
}

void QuizMaker::modify(Quiz* toModify)
{
    int choise;
    string question;
    string answer[5];
    int rightAnswer;
    int mark;
    cout << "Що ви хочете поміняти?" << endl;
    cout << "Питання [1]" << endl;
    cout << "Відповіді [2]" << endl;
    cout << "Правильну відповідь [3]" << endl;
    cout << "Оцінку за питання [4]" << endl;
    cout << " : ";
    cin >> choise;
    switch(choise)
    {
        case 1:
            cout << "Введіть питання: ";
            cin.ignore();
            getline(cin, question);
            toModify->setQuestion(question);
            break;
        case 2:
            cout << "Введіть відповіді:" << endl;
            for(int i=0; i<5; i++)
            {
                cout << i << ": ";
                cin >> answer[i];
            }
            toModify->setAnswers(answer);
            break;
        case 3:
            cout << "Введіть відповідь: ";
            cin >> rightAnswer;
            toModify->setRightAnswer(rightAnswer);
            break;
        case 4:
            cout << "Введіть відповідь: ";
            cin >> mark;
            toModify->setMark(mark);
            break;
        default:
            break;
    }
}
