#ifndef QUIZ_FILEREADER_H
#define QUIZ_FILEREADER_H

#include "QuizMaker.h"

class FileReader {
public:
    QuizMaker questionList;
    int size;

    FileReader();
    void read();
};


#endif //QUIZ_FILEREADER_H
