#ifndef POINTCLASS_POINT_H
#define POINTCLASS_POINT_H

#include <iostream>

using namespace std;

class Point {
private:
    int xAxis;
    int yAxis;
public:
    Point(int xAxis, int yAxis);
    Point();
    Point(const Point & otherPoint);
    double getDistanceToCenter();
    double getDistance(Point &point);
    bool isInCircle(Point centre, int radiusInner, int radiusOuter);
    Point operator+(Point &point);
    Point operator*(Point &point);
    bool operator<(Point &point);
    bool operator>(Point &point);

    friend ostream&operator<<(ostream &output, Point point);
    friend istream&operator>>(istream &input, Point &point);
};


#endif //POINTCLASS_POINT_H
