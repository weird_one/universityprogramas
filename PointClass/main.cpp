#include <iostream>
#include "Point.h"

void fillTheMass(Point *pointMass, int size);
void showTheMass(Point *pointMass, int size);
void showTheNearestToCenter(Point *pointMass, int size);
void showTheMostRemotePoints(Point *pointMass, int size);
void showPointsInTheCircle(Point *pointMass, int size);

using namespace std;

int main() {
    int size;
    cout << "Введіть розмірність масиву: ";
    cin >> size;
    Point* massOfPoints = new Point[size];
    fillTheMass(massOfPoints, size);
    showTheNearestToCenter(massOfPoints, size);
    showTheMostRemotePoints(massOfPoints, size);
    showPointsInTheCircle(massOfPoints, size);
    return 0;
}

void showPointsInTheCircle(Point *pointMass, int size) {
    cout << "Введіть центр кола: ";
    Point centre;
    cin >> centre;
    cout << "Введіть менший радіус: ";
    int lessRadius;
    cin >> lessRadius;
    cout << "Введіть більший радіус: ";
    int greaterRadius;
    cin >> greaterRadius;

    cout << "Наступні точки в кільці: " << endl;
    for(int i=0; i<size; i++){
        if(pointMass[i].isInCircle(centre, lessRadius, greaterRadius)){
            cout << pointMass[i];
        }
    }
}

void showTheMostRemotePoints(Point *pointMass, int size) {
    int indicatorMin = 0;
    int indicatorMax = 0;
    double largestDistance = pointMass[0].getDistance(pointMass[0]);
    for(int i=0; i<size; i++)
    {
        for(int j=i+1; j<size; j++)
        {
            if(pointMass[i].getDistance(pointMass[j]) > largestDistance){
                largestDistance = pointMass[i].getDistance(pointMass[j]);
                indicatorMin = i;
                indicatorMax = j;
            }
        }
    }

    cout << endl;
    cout << "Найбільш віддалені точки це: "
         << pointMass[indicatorMin]
         << pointMass[indicatorMax];

    cout << "Їх сумма: "
         << pointMass[indicatorMin] + pointMass[indicatorMax]
         << endl;

    cout << "Їх добуток: "
         << pointMass[indicatorMin] * pointMass[indicatorMax]
         << endl;

}

void showTheNearestToCenter(Point *pointMass, int size) {
    double min = pointMass[0].getDistanceToCenter();
    int indicator = 0;
    for(int i=0; i<size; i++)
    {
        if(pointMass[i].getDistanceToCenter() < min){
            min = pointMass[i].getDistanceToCenter();
            indicator = i;
        }
    }
    cout << endl;
    cout << "Найближча точка до початку координат: " << pointMass[indicator] << endl;

}

void showTheMass(Point *pointMass, int size) {
    for(int i=0; i<size; i++)
    {
        cout << pointMass[i];
    }

}

void fillTheMass(Point *pointMass, int size) {
    cout << "Введіть координати точок: " << endl;
    for(int i=0; i<size; i++)
    {
        cout << i+1 << ": ";
        cin >> pointMass[i];
    }
}