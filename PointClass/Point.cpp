#include <iostream>
#include <cmath>
#include "Point.h"

Point::Point(int xAxis, int yAxis) : xAxis(xAxis), yAxis(yAxis) {

}

Point::Point()
{
    xAxis = 0;
    yAxis = 0;
}

ostream &operator<<(ostream &output, Point point) {
    output << "Координата x: " << point.xAxis << " координата у: " << point.yAxis << endl;
    return output;
}

istream &operator>>(istream &input, Point& point) {
    cout << "Введіть координату х: ";
    input >> point.xAxis;
    cout << "Введіть координату y: ";
    input >> point.yAxis;
    return input;
}

Point Point::operator+(Point &point) {
    Point newPoint;
    newPoint.xAxis = xAxis + point.xAxis;
    newPoint.yAxis = yAxis + point.yAxis;
    return newPoint;
}

Point Point::operator*(Point &point) {
    Point newPoint;
    newPoint.xAxis = xAxis * point.xAxis;
    newPoint.yAxis = yAxis * point.yAxis;
    return newPoint;
}

bool Point::operator<(Point &point)
{
    return getDistanceToCenter() < point.getDistanceToCenter();
}

double Point::getDistanceToCenter()
{
    int xAxisSquere = xAxis * xAxis;
    int yAxisSquere = yAxis * yAxis;
    return sqrt(xAxisSquere + yAxisSquere);
}

bool Point::operator>(Point &point)
{
    return getDistanceToCenter() > point.getDistanceToCenter();
}

double Point::getDistance(Point &point) {
    double xVectorAxis = point.xAxis - xAxis;
    double yVectorAxis = point.yAxis - yAxis;
    double distance = sqrt((xVectorAxis*xVectorAxis) + (yVectorAxis*yVectorAxis));
    return distance;
}

bool Point::isInCircle(Point centre, int radiusInner, int radiusOuter)
{
    if(getDistance(centre) > radiusInner && getDistance(centre) < radiusOuter)
    {
        return true;
    }
    return false;
}

Point::Point(const Point &otherPoint) {
    xAxis = otherPoint.xAxis;
    yAxis = otherPoint.yAxis;
}
