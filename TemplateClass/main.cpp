#include <iostream>
#include "DynamicArray.h"

int main() {
    DynamicArray<int> intArray;
    DynamicArray<string> stringArray;
    int toCin;
    string toCinStr;
    bool wantToEnter = true;
        cout << "Введіть елементи масиву цілих чисел" << endl;
        while (wantToEnter) {
            cout << "елемент: ";
            cin >> toCin;
            intArray.add(toCin);

            cout << "Бажаєте ввести ще елемент? [1/0]";
            cin >> toCin;
            wantToEnter = toCin == 1;
        }

        wantToEnter = true;

        cout << "Введіть елементи масиву cтрічок" << endl;
        while (wantToEnter) {
            cout << "елемент: ";
            cin >> toCinStr;
            stringArray.add(toCinStr);

            cout << "Бажаєте ввести ще елемент? [1/0]";
            cin >> toCin;
            wantToEnter = toCin == 1;
        }

        cout << "Найменший і найбільший елементи масиву цілих чисел : min: "
             << intArray.getMin()
             << " ,max: "
             << intArray.getMax()
             << endl;

        cout << "Найменший і найбільший елементи масиву стрічок : min: "
             << stringArray.getMin()
             << " ,max: "
             << stringArray.getMax()
             << endl;

        cout << "Елемент що найчастіше зустрічається в цілих числах: "
             << intArray.findMost()
             << endl;

        cout << "Елемент що найчастіше зустрічається в стрічках: "
             << stringArray.findMost()
             << endl;

    return 0;
}