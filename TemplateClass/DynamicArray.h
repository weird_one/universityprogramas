#ifndef TEMPLATECLASS_DYNAMICARRAY_H
#define TEMPLATECLASS_DYNAMICARRAY_H

#include <iostream>
#include "ArrayOutOfBoundExeption.h"

using namespace std;

template <class Val>
class DynamicArray
{
private:
    Val* array;
    int size;
    Val min;
    Val max;
    int count;

    void findMinMax()
    {
        Val minElement = array[0];
        Val maxElement = array[0];
        for(int i=0; i<count; i++)
        {
            if(array[i] < minElement)
            {
                minElement = array[i];
            }
            if(array[i] > maxElement)
            {
                maxElement = array[i];
            }
        }
        max = maxElement;
        min = minElement;
    }



public:

    DynamicArray()
    {
        count = 0;
        size = 10;
        array = new Val[size];
    }

    DynamicArray(int size) :
            size(size)
    {
        array = new Val[size];
        count =0;
    }

    DynamicArray(Val *array, int size) :
            array(array),
            size(size)
    {
        count = 0;
    }

    void add(Val val)
    {
        if(count == size)
        {
            increaseArray();
        }
        array[count] = val;
        count++;
    }

    void add(int index, Val val)
    {
        if(index >= size)
        {
            increaseArray();
        }
        array[index] = val;
    }

    void increaseArray()
    {
        int newSize = size + 10;
        Val* newArr = new Val[size];
        for(int i=0; i<count; i++)
        {
            newArr[i] = array[i];
        }
        delete array;
        array = newArr;
        size = newSize;
    }

    void increaseArray(int otherSize)
    {
        int newSize = size + otherSize;
        Val newArr = new Val[size];
        for(int i=0; i<size; i++)
        {
            newArr[i] = array[i];
        }
        delete array;
        array = newArr;
        size = newSize;
    }

    Val findMost()
    {
        int maxIndex = 0;
        int countTemp = 0;
        int maxCount = 0;
        for(int i=0; i<count; i++)
        {
            for(int j=i; j<count; j++)
            {
                if(array[i] == array[j])
                {
                    countTemp++;
                }
            }
            if(maxCount < countTemp)
            {
                maxCount = countTemp;
                maxIndex = i;
            }
            countTemp  = 0;
        }
        return array[maxIndex];
    }

    Val& operator[](int index)
    {
        if(index >= size)
        {
            throw ArrayOutOfBoundExeption();
            increaseArray();
        }
        return array[index];
    }

    DynamicArray&operator = (DynamicArray& secondArray)
    {
        delete array;
        size = secondArray.size;
        array = new Val[size];
        for(int i=0; i<size; i++)
        {
            array[i] = secondArray.array[i];
        }
        return *this;
    }

    int getSize() const {
        return size;
    }

    Val getMin()
    {
        findMinMax();
        return min;
    }

    Val getMax()
    {
        findMinMax();
        return max;
    }

    ~DynamicArray()
    {
        delete[] array;
    }

};

#endif //TEMPLATECLASS_DYNAMICARRAY_H
