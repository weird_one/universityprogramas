#include "gtest/gtest.h"
#include "../table/HashTable.h"


void doThing(Element *element)
{
    element->val += " Hello";
}

TEST(HashTableTests, AddCollisTest)
{
    HashTable table;
    table.add("Key", "Monkey");
    table.add("Yek", "Tiger");
    table.add("Bird", "Falcon");
    table.deleteByKey("Bird");
    table.add("Bird", "Hawk");
    ASSERT_EQ("Hawk", table.getValueByKey("Bird"));
    ASSERT_EQ("Tiger", table.getValueByKey("Yek"));
}

TEST(HashTableTests, ReplaceTest)
{
    HashTable table;
    table.add("Key", "Monkey");
    table.add("Bird", "Hawk");
    table.replace("Bird", "Falcon");
    ASSERT_EQ("Falcon", table.getValueByKey("Bird"));
}

TEST(HashTableTests, SearchTest)
{
    HashTable table;
    table.add("Key", "Monkey");
    table.add("Yek", "Tiger");
    table.add("Bird", "Falcon");
    table.add("Koo", "Moo");
    string elementVal;
    elementVal = table.findByKey("Bird").val;
    ASSERT_EQ("Falcon", elementVal);
}

TEST(HashTableTests, IterateTest)
{
    HashTable table;
    table.add("Key", "Monkey");
    table.add("Yek", "Tiger");
    table.add("Bird", "Falcon");
    table.add("Koo", "Moo");
    table.iterate(doThing);
    ASSERT_EQ("Moo Hello", table.getValueByKey("Koo"));
}