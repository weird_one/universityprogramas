#ifndef HASHTABLE_LINKDLIST_H
#define HASHTABLE_LINKDLIST_H


#include "Element.h"

class LinkedList
{
public:
    Element* head;
    LinkedList();
    void add(string key, string val);
    void deleteElement(string key);
    void replaceByKey(string key, string val);
    Element search(string key);
    void iterate(void (*doThing)(Element*));
    string getValueByKey(string key);
};


#endif //HASHTABLE_LINKDLIST_H
