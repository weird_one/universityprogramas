#include "LinkedList.h"

void LinkedList::add(string key, string val)
{
    head = new Element(key, val, head);
}

LinkedList::LinkedList()
{
    head = nullptr;
}

void LinkedList::deleteElement(string key)
{

    Element* elementPrevious;
    Element* elementNext;
    Element* elementToDelete = head;
    if(head != nullptr)
    {
        if (head->key == key)
        {
            head = head->next;
        }
        else
        {
            while (elementToDelete->next != nullptr)
            {
                if(elementToDelete->next->key != key)
                {
                    elementToDelete = elementToDelete->next;
                }
                else
                {
                    break;
                }
            }

            if(elementToDelete != nullptr)
            {
                elementPrevious = elementToDelete;
                elementToDelete = elementToDelete->next;
                elementNext = elementToDelete->next;

                delete elementToDelete;
                elementPrevious->next = elementNext;
            }
        }
    }
    else
    {
        cout << "You dont have such element" << endl;
    }
}

void LinkedList::replaceByKey(string key, string val)
{
    Element* elementToReplace = head;
    while(elementToReplace!= nullptr)
    {
        if (elementToReplace->key != key)
        {
            elementToReplace = elementToReplace->next;
        }
        else
        {
            break;
        }

    }
    if(elementToReplace != nullptr)
    {
        elementToReplace->val = val;
    }
    else
    {
        cout << "You dont have such element" << endl;
    }
}

string LinkedList::getValueByKey(string key)
{
    if (head != nullptr)
    {
        Element *findElement = head;
        while (findElement!= nullptr)
        {
            if(findElement->key != key)
            {
                findElement = findElement->next;
            }
            else
            {
                break;
            }
        }
        if(findElement != nullptr)
        {
            return findElement->val;
        }
    }
    else
    {
        return "Your table is empty";
    }
}

Element LinkedList::search(string key)
{

    Element* findElement = head;
    while(findElement!= nullptr)
    {
        if(findElement->key != key)
        {
            findElement = findElement->next;
        }
        else
        {
            break;
        }
    }
    if(findElement!= nullptr)
    {
        return *findElement;
    }
    else
    {
        cout << "You dont have such element, instead of exeption catch this fake element" << endl;
        Element *fakeElement = new Element("Empty", "Empty");
        return *fakeElement;
    }
}

void LinkedList::iterate(void (*doThing)(Element*))
{
    void (*func)(Element*);
    func = doThing;
    Element* elementToIterate = head;
    while(elementToIterate != nullptr)
    {
        func(elementToIterate);
        elementToIterate = elementToIterate->next;
    }
}


