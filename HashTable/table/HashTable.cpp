#include "HashTable.h"

int HashTable::hash(string key)
{
    int value = 0;
    for ( int i = 0; i < key.length(); i++)
    {
        value += key[i];
    }
    return value % size;
}

HashTable::HashTable()
{
    size = 10;
    hashTable = new LinkedList*[size];
    for(int i=0; i<size; i++)
    {
        hashTable[i] = nullptr;
    }
}

HashTable::HashTable(int size)
{
    this->size = size;
    hashTable = new LinkedList*[size];
}

void HashTable::add(string key, string val)
{
    int index;
    index = hash(key);
    if(hashTable[index] == nullptr) {
        hashTable[index] = new LinkedList;
    }
    hashTable[index]->add(key, val);
}

string HashTable::getValueByKey(string key)
{
    int index;
    index = hash(key);
    if(hashTable[index]->head != nullptr)
    {
        return hashTable[index]->getValueByKey(key);
    }
    return "No such element";
}

void HashTable::deleteByKey(string key)
{
    int index;
    index = hash(key);
    if(hashTable[index] != nullptr)
    {
        hashTable[index]->deleteElement(key);
    }
}

Element HashTable::findByKey(string key)
{
    int index;
    index = hash(key);
    if(hashTable[index] != nullptr && hashTable[index]->head != nullptr)
    {
        return  hashTable[index]->search(key);
    }
    return Element("", "No such element");
}

void HashTable::replace(string key, string val)
{
    int index;
    index = hash(key);
    if(hashTable[index] != nullptr && hashTable[index]->head != nullptr)
    {
        hashTable[index]->replaceByKey(key, val);
    }
}

void HashTable::iterate(void (*doThing)(Element*))
{
    void (*func)(Element*);
    func = doThing;
    for(int i=0; i<size; i++)
    {
        if(hashTable[i] != nullptr && hashTable[i]->head != nullptr)
        {
            hashTable[i]->iterate(func);
        }
    }
}

HashTable::~HashTable() {
    for(int i=0 ;i<size; i++)
    {
        delete hashTable[i];
    }
    delete hashTable;
}
