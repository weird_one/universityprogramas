#ifndef HASHTABLE_TABLE_H
#define HASHTABLE_TABLE_H

#include "LinkedList.h"
#include <iostream>

using namespace std;

class HashTable
{
private:
    int size;
    LinkedList** hashTable;
public:
    HashTable();
    HashTable(int size);
    int hash(string key);
    void add(string key, string val);
    void deleteByKey(string key);
    string getValueByKey(string key);
    Element findByKey(string key);
    void replace(string key, string val);
    void iterate(void (*doThing)(Element*));
    ~HashTable();
};


#endif //HASHTABLE_TABLE_H
