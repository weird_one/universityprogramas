#include "Element.h"

Element::Element(const string &key, const string &val) :
        key(key),
        val(val)
{

}

Element::Element(const string &key, const string &val, Element* next) :
        key(key),
        val(val),
        next(next)
{

}
