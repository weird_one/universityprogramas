#ifndef HASHTABLE_ELEMENT_H
#define HASHTABLE_ELEMENT_H

#include <iostream>

using namespace std;

class Element
{
public:
    string key;
    string val;
    Element* next;
    Element(const string &key, const string &val);
    Element(const string &key, const string &val, Element* next);
};


#endif //HASHTABLE_ELEMENT_H
